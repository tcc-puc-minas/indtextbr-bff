const { AuthService, LoggerService } = require("indtextbr-utils");
const { isError } = require("joi");
const auth = new AuthService().getInstance();
const logger = new LoggerService().getInstance();
const repository = require("../repositories/user.repository");
exports.login = async (req, res, next) => {
  try {
    const user = await repository.getByCredentials(req.body);
    if (!user) {
      return res.status(401).send({
        success: false,
        message: "E-mail e/ou senha inválidos",
      });
    }
  
    const data = {
      id: user.id,
      email: user.email,
      name: user.name,
      profile: {
        id: user.profiles.id,
        name: user.profiles.name,
        roles: user.profiles.roles.split(','),
      }
      
    };
    const token = await auth.generateToken({ data });
  
    return res.status(200).send({
      token,
      user: data,
    });
  } catch (error) {
      logger.error(error);
      res.status(500).send({
          message: "Erro interno! Entre em contato com o administrador do sistema"
      })
  }
};
