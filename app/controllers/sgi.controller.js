const repository = require("../repositories/sgi.repository");
const { LoggerService } = require("indtextbr-utils");
const logger = new LoggerService().getInstance();
const moment = require("moment-timezone");
module.exports.orders = async (req, res, next) => {
  try {
    const orders = await repository.getOrders();
    orders.sort((a, b) => {
      return new Date(b.update_at) - new Date(a.update_at)
    })
    const dto = orders.map((order) => {

      return {
        id: order.id,
        status: order.status.name,
        statusId: order.status.id,
        item: order.item,
        quantity: order.quantity,
        purchaser: order.purchaserName,
        priority: order.priority,
        color: order.color
      }
    });

    return res.send(dto);
  } catch (err) {
    logger.error(err);
    res.status(500).send({
      message: "Erro interno! Entre em contato com o administrador do sistema",
    });
  }
};

module.exports.orderById = async (req, res, next) => {

  try {
    const { id } = req.params;
    const response = await repository.getOrderById(id);
    if (Object.keys(response).length === 0) {
      return res.status(404).send({
        message: 'Pedido não encontrado'
      })
    }
    console.log(response);
    const formatTimezone = moment(response.update_at).tz('America/Sao_Paulo').format('DD/MM/YYYY HH:mm');
    return res.send({
      id: response.id,
      item: response.item,
      purchaserName: response.purchaserName,
      priority: response.priority,
      quantity: response.quantity,
      status: response.status.name,
      statusId: response.status.id,
      departmentName: response.status.department.name,
      departmentId: response.status.department.id,
      details: response.details,
      updateAt: formatTimezone,

    });
  } catch (err) {
    logger.error(err);
    res.status(500).send({
      message: "Erro interno! Entre em contato com o administrador do sistema",
    });
  }
};

module.exports.changeOrderPriority = async (req, res, next) => {
  try {
    const { id } = req.params;
    const { priority } = req.body
    const response = await repository.changeOrderPriority({ id, priority });
    if (response === true) {
      return res.status(204).send();
    } else {
      return res.status(400).send({
        message: 'Usuário inválido'
      });
    }

  } catch (err) {
    logger.error(err);
    res.status(500).send({
      message: "Erro interno! Entre em contato com o administrador do sistema",
    });
  }
};

module.exports.status = async (req, res, next) => {
  try {
    const response = await repository.getStatus();
    return res.send(response);
  } catch (err) {
    logger.error(err);
    res.status(500).send({
      message: "Erro interno! Entre em contato com o administrador do sistema",
    });
  }
};

module.exports.departments = async (req, res, next) => {
  try {
    const response = await repository.getDepartments();
    return res.send(response);
  } catch (err) {
    logger.error(err);
    res.status(500).send({
      message: "Erro interno! Entre em contato com o administrador do sistema",
    });
  }
};