const { LoggerService, AuthService } = require("indtextbr-utils");
const auth = new AuthService().getInstance();
const logger = new LoggerService().getInstance();
const { sendEmailCastrado } = require('../services/email.service');
const userRepository = require("../repositories/user.repository");
const profileRepository = require("../repositories/profile.repository");

module.exports.create = async (req, res, next) => {

  const data = req.body;

  try {
    //TODO: Remover validacao de negocio do controller
    const isAnUserExistent = await userRepository.isExistent(data.email);
    console.log('isAnUserExistent:', isAnUserExistent);
    if (isAnUserExistent) {
      return res
        .status(400)
        .send({ success: false, message: "E-mail já registrado" });
    }

    //TODO: Remover validacao de negocio do controller
    const isAProfileExistent = await profileRepository.isExistent(data.profileId);
    if (!isAProfileExistent) {
      return res
        .status(400)
        .send({ success: false, message: "Perfil selecionado inexistente" });
    }

    const keyWord = auth.generatePassword();


    const password = auth.createHash(keyWord);


    const response = await userRepository.create({
      name: data.name,
      email: data.email,
      password,
      profile_id: data.profileId,
    });

    //TODO: enviar email
    await sendEmailCastrado({
      name: data.name,
      email: data.email,
      password: keyWord
    });

    return res.status(201).send({
      name: response.name,
      email: response.email
    });

  } catch (error) {

    logger.error(error);
    res.status(500).send({
      message: "Erro interno! Entre em contato com o administrador do sistema"
    });

  }
};

module.exports.list = async (req, res, next) => {
  const ignored_users = process.env.IGNORED_USERS_IN_GET.split(',');
  const listFromDB = await userRepository.getAll();
  return res.status(200).send(listFromDB.filter((item) => {
    return !ignored_users.includes(item.email);
  }).map((item) => {
    const { id, name, email, status, profiles } = item;
    return {
      id,
      name,
      email,
      status,
      profileId: profiles.id,
      profileName: profiles.name
    }
  }));
}

module.exports.changeStatus = async (req, res, next) => {

  const { id } = req.params;
  const { status } = req.body;

  try {

    console.log('id: ', id);
    console.log('req.user.id: ', req.user.id);

    if (Number(id) === req.user.id) {

      return res.status(400).send({
        success: false,
        message: 'operação não pode ser realizada',
      });

    }

    await userRepository.updateStatus({
      id,
      status,
    });

    return res.status(200).send({
      success: true,
      message: 'usuario alterado com sucesso!',
    });

  } catch (error) {

    logger.error(error);

    res.status(500).send({
      message: "Erro interno! Entre em contato com o administrador do sistema"
    });

  }


}





