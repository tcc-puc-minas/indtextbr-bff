module.exports = (sequelize, DataTypes) => {
  const profiles = sequelize.define(
    "profiles",
    {

      id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        unique: true,
        allowNull: false,
      },
      name: {
        type: DataTypes.TEXT,
        allowNull: false,
      },
      roles: {
        type: DataTypes.STRING,
      },
    },
    {
      underscored: true,
      paranoid: true,
      timestamps: false,
    }
  );

  profiles.associate = function (models) {
    profiles.hasMany(models.users, {
      foreignKey: "profile_id",
      as: "users",
    });
  };
  return profiles;
};
