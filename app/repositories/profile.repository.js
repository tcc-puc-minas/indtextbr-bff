"use strict";
const { profiles } = require("../models");

exports.getAll = async () => {
  return profiles.findAll({
    where: {}
  });
};

exports.getById = async (id) => {
  const response = await profiles.findOne({
    where: { id: id }
  });
  return response;
};

exports.isExistent = async (id) => {
  const response = await profiles.findOne({ where: { id: id } });
  return response ? true : false;
};
