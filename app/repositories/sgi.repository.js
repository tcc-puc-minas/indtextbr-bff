const { SGICache } = require('../services/cache.service');
const cache = new SGICache();

const path = require("path");
const findStatus = (order) => {
  const status = cache.get().status.find((s) => order.status_id === s.id);
  const department = findDepartment(status);

  return { ...status, department };
};

const findDepartment = (status) => {
  const department = cache.get().departments.find(
    (dp) => dp.id === status.department_id
  );
  return department;
};
module.exports.getOrders = async () => {
  const response = cache.get().orders.map((order) => {
    const status = findStatus(order);
    return {
      ...order,
      status,
    };
  });

  return response;
};

module.exports.getOrderById = async (id) => {
  const order = cache.get().orders.find((x) => x.id == id);
  if(order === undefined) {
      return {};
  }
  const status = findStatus(order);
  return {
    ...order,
    status,
  };
};

module.exports.getStatus = async () => {
  const response = cache.get().status.map(s => {
    const department = findDepartment(s);
    return {
        ...s, department
    }
  });
  
  return response;
};

module.exports.getDepartments = async () => {
  const response = cache.get().departments;
  return response;
};

module.exports.changeOrderPriority = async ({id, priority}) => {
    let sgi = cache.get();
    let item = sgi.orders.find(order => order.id == id);
    if(item === undefined){
        return false;
    }
    item.priority = priority;
    const index = sgi.orders.findIndex(order => order.id === id);
    sgi.orders[index] = item;
    cache.set(sgi);
    return true;

}

