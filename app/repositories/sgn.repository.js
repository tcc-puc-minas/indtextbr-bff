const { SGNCache } = require('../services/cache.service');
const cache = new SGNCache();
const path = require("path");

module.exports.getStandards = async () => {
  const response = cache.get().standards.map((standard) => {
    
    return {
      ...standard,
    };
  });

  return response;

};

