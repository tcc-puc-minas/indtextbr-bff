"use strict";

const { AuthService } = require("indtextbr-utils");
const auth = new AuthService().getInstance();
const { users } = require("../models");
const moment = require('moment-timezone');

exports.getByCredentials = async ({ email, password }) => {
  const response = await users.findOne({
    where: { email: email, password: auth.createHash(password), status: true },
    include: ["profiles"],
  });
  return response;
};

exports.create = async ({ name, email, profile_id, password }) => {
  const response = await users.create({
    name,
    email,
    password,
    profile_id,
  });
  return response;
};

exports.getAll = async () => {
  const response = await users.findAll({ include: ["profiles"] });
  return response;
};

exports.getById = async (id) => {
  const response = await users.findOne({
    where: { id: id },
    include: ["profiles"],
  });
  return response;
};

exports.updateStatus = async ({ status, id }) => {
  const current = moment();
  return users.update(
    { status: status, updated_at: current },
    { where: { id: id } }
  )
}

exports.deleteByEmail = async (email) => {
  return users.destroy({
    where: {
      email
    }
  });
}

exports.isExistent = async (email) => {
  const response = await users.findOne({ where: { email: email } });
  return response ? true : false;
};
