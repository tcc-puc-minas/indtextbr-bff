const { Router } = require("express");
const { name, version } = require('../../package.json');

const authRouteV1 = require('./v1/auth.route');
const sgiRouteV1 = require('./v1/sgi.route');
const sgnRouteV1 = require('./v1/sgn.route');
const userManagerRouteV1 = require('./v1/user-manager.route');

module.exports = (app) => {
    app.get("/", (req, res, next) => {
        res.send({ name, version })
    })

    const v1 = Router();
    authRouteV1(v1);
    sgiRouteV1(v1);
    sgnRouteV1(v1);
    userManagerRouteV1(v1);
    app.use('/v1', v1);
}