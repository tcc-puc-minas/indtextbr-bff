const Joi = require("joi");
const { validateDto } = require("../../utils/validator.utils");
const authController = require('../../controllers/auth.controller');

module.exports = (router) => {
  router.route("/login").post(
    validateDto("body", {
      email: Joi.string().required().messages({ 'any.required': "E-mail é obrigatório" }),
      password: Joi.string()
        .required()
        .messages({ 'any.required': 'Senha é obrigatório' }),
    }),
    authController.login
  );
};
