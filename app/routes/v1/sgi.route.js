const Joi = require("joi");
const { validateDto } = require("../../utils/validator.utils");
const authorize = require("../../services/authorization.service");
const controller = require("../../controllers/sgi.controller");
const flag = "sgi";

module.exports = (router) => {
  router.route("/sgi/orders").get(authorize(flag), controller.orders);

  router.route("/sgi/orders/:id").get(authorize(flag), controller.orderById);

  router.route("/sgi/orders/:id/priority").put(
    validateDto("body", {
      priority: Joi.string()
        .valid('baixa', 'media', 'alta')
        .required()
        .messages({
          "any.required": "Campo priority é necessário",
          "any.valid":
            "E necessário definir prioridade como baixa, media ou alta",
        }),
    }),
    authorize(flag),
    controller.changeOrderPriority
  );

  router.route("/sgi/departments").get(authorize(flag), controller.departments);

  router.route("/sgi/status").get(authorize(flag), controller.status);
};
