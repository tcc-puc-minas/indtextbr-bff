// const Joi = require("joi");
// const { validateDto } = require("../../utils/validator.utils");
const authorize = require("../../services/authorization.service");
const controller = require("../../controllers/sgn.controller");
const flag = "sgn";

module.exports = (router) => {
  router.route("/sgn/standards").get(authorize(flag), controller.standards);
};
