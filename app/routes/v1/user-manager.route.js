const Joi = require("joi");
const { AuthService } = require('indtextbr-utils');
const { validateDto } = require("../../utils/validator.utils");
const userController = require('../../controllers/user.controller');
const authorize = require("../../services/authorization.service");
const flag = 'user-manager';

module.exports = (router) => {

  // router.route("/user")

  router.route("/user").post(
    authorize(flag),
    validateDto("body", {
      name: Joi
        .string()
        .min(5)
        .max(30)
        .required()
        .messages({
          'any.required': `"nome" é um campo obrigatório`,
          'string.empty': `"nome" não deve ser vazio`,
          'string.min': `"nome" não deve ter menos que {#limit} caracteres`,
          'string.max': `"nome" não deve ter mais que {#limit} caracteres`,
        }),
      email: Joi
        .string()
        .email()
        .required()
        .messages({
          'any.required': `"email" é um campo obrigatório`,
          'string.empty': `"email" não deve ser vazio`,
          'string.email': `"email" deve ser um email válido`,
        }),
      profileId: Joi.number().integer().required().messages({
        'any.required': `"perfil" é um campo obrigatório`,
        'number.base': `"perfil" deve ser um número`,
        'number.integer': `"perfil" deve ser um número válido`
      })

    }),
    userController.create
  ).get(
    authorize(flag),
    userController.list
  );

  router.route('/user/:id').put(
    authorize(flag),
    validateDto("params", {
      id: Joi.number().integer().required().messages({
        'any.required': `"id" é um campo obrigatório`,
        'number.base': `"id" deve ser um número`,
        'number.integer': `"id" deve ser um número válido`
      })
    }),
    validateDto("body", {
      status: Joi.bool().required().messages({
        'any.required': `"status" é um campo obrigatório`,
        'boolean.base': `"status" deve ser um boolean`,
      })
    }),
    userController.changeStatus
  )

  // router.route('/user/:id/enable').put(
  //   authorize(flag),
  //   validateDto("body", {
  //     status: Joi.
  //   })
  // )


};
