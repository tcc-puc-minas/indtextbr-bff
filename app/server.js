const { LoggerService } = require("indtextbr-utils");
const logger = new LoggerService().getInstance();
const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const morgan = require("morgan");
const routes = require("./routes/index");
const port = process.env.PORT || 3001;
const app = express();
app.use(morgan("combined"));
app.use(bodyParser.json({ limit: "2mb" }));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors());

routes(app);

app.listen(port, () => {
  logger.info(`Server listening on port ${port}`);
});
