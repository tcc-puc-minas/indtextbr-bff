const { LoggerService, AuthService } = require("indtextbr-utils");
const auth = new AuthService().getInstance();
const logger = new LoggerService().getInstance();
const repository = require("../repositories/user.repository");

const authorize = (flag) => async (req, res, next) => {
  try {
    const { token } = req.headers;

    if (!token) {
      return res.status(401).send({
        success: false,
        message: "Token inválido",
      });
    }

    //TODO: Tratar quando token for invalido
    const data = await auth.decodeToken(token);

    const user = await repository.getById(data.id);

    const roles = user.profiles.roles.split(",");

    const response = await auth.authorize({ roles, flag });

    if (response.success === false) {

      return res.status(response.status).send({ success: response.success, response });

    } else {

      req.user = user;

      next();

    }
  } catch (err) {
    logger.error(err);
    res.status(500).send({
      message: "Erro interno! Entre em contato com o administrador do sistema"
    })
  }
};

module.exports = authorize;
