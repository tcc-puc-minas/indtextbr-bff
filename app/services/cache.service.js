class SGICache {
    get = () => {
        return global.CACHE.get('SGI')
    }
    set = (data) => {
        return global.CACHE.set('SGI', data);
    }
}

class SGNCache {
    get = () => {
        return global.CACHE.get('SGN');
    }
    set = (data) => {
        return global.CACHE.set('SGN', data);
    }
}

module.exports = {
    SGICache,
    SGNCache,
}

