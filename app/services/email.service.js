

const { LoggerService, MailService } = require("indtextbr-utils");

const mailService = new MailService().getInstance();

const logger = new LoggerService().getInstance();

const repository = require("../repositories/user.repository");


const sendEmailCastrado = async ({ name, password, email, }) => {

  const html = `<h1>Usuário ${name} criado com sucesso!</h1></br></br><h3>login:${email}</h3></br><h3>login:${password}</h3>`;

  return mailService.send({ to: email, subject: 'Criação de Usuários', html });

}

module.exports = {
  sendEmailCastrado,
};