const Joi = require("joi");
const createDetails = (error) => {
  return error.details.reduce((acc, item) => {
    return [
      ...acc, item.message
    ];
  }, []);
}

exports.validateDto = (type, params) => (req, res, next) => {


  const schema = Joi.object().keys(params);

  const { value, error } = schema.validate(req[type], {
    allowUnknown: true,
  });

  req[type] = value;

  return error ? res.status(422).send({
    detalhes: [...createDetails(error)]
  }) : next();

};