'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {

    return Promise.all([

      queryInterface.createTable('profiles', {
        id: {
          allowNull: false,
          autoIncrement: true,
          primaryKey: true,
          type: Sequelize.INTEGER
        },
        name: {
          allowNull: false,
          type: Sequelize.TEXT
        },
        roles: {
          allowNull: true,
          type: Sequelize.TEXT
        }
      }),

      queryInterface.createTable('users', {
        id: {
          allowNull: false,
          autoIncrement: true,
          primaryKey: true,
          type: Sequelize.INTEGER
        },
        email: {
          allowNull: false,
          type: Sequelize.TEXT
        },
        name: {
          allowNull: false,
          type: Sequelize.TEXT
        },
        password: {
          allowNull: false,
          type: Sequelize.TEXT
        },
        status: {
          allowNull: false,
          type: Sequelize.BOOLEAN,
          defaultValue: true
        },
        profile_id: {
          allowNull: false,
          type: Sequelize.INTEGER,
          references: { model: 'profiles', key: 'id' },
          onDelete: 'CASCADE',
        },
        created_at: {
          type: Sequelize.DATE,
          defaultValue: Sequelize.literal('NOW()')
        },
        updated_at: {
          type: Sequelize.DATE,
          defaultValue: Sequelize.literal('NOW()')
        }
      }),

    ]);

  },

  down: async (queryInterface, Sequelize) => {

    return Promise.all([
      queryInterface.dropTable('users'),
      queryInterface.dropTable('profiles'),
    ])

  }
};
