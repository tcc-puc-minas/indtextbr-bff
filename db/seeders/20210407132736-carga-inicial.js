'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert('profiles', [
      {
        name: 'Gestor',
        roles: 'sgi,user-manager,sca'
      },
      {
        name: 'Segurança do Trabalho',
        roles: 'sgn'
      },
      {
        name: 'Compliance',
        roles: 'sgn'
      },
      {
        name: 'Supervisor',
        roles: 'sgi'
      }
    ])

    await queryInterface.bulkInsert('users', [
      {
        name: 'Administrador',
        email: 'administrador@indtextbr.com.br',
        password: 'a747adde1c03fb3d8c651a934802d577', //123456 + key
        profile_id: 1
      }
    ])
    
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('users', null, {});
    await queryInterface.bulkDelete('profiles', null, {});
  }
};
