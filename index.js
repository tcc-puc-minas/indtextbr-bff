const { version, name } = require("./package.json");
const { LoggerService, AuthService, MailService } = require('indtextbr-utils');

const NodeCache = require("node-cache");
const sgiMock = require('./mocks/sgi-db.json');
const sgnMock = require('./mocks/sgn-db.json');

global.CACHE = new NodeCache();
global.CACHE.set('SGI', sgiMock);
global.CACHE.set('SGN', sgnMock);

const environment = process.env.NODE_ENV || 'development';

if (environment === 'development')
    require('dotenv').config();

new LoggerService({
    logLevel: process.env.LOG_LEVEL,
    logName: 'indtextbr-bff',
});

new AuthService({
    saltKey: process.env.AUTH_KEY
});


new MailService({
    fromMail: process.env.SENDGRID_EMAIL,
    apiKey: process.env.SENDGRID_API_KEY
});

const logger = new LoggerService().getInstance();


require('./app/server');

logger.info(
    `<<< ${name} v${version} was started in ${process.env.NODE_ENV || 'development'} environment >>>`
);